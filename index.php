<!DOCTYPE html>
<html>
<head>
  <title>ExactControl Automação LTDA</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="A ExactControl Automação é Distribuidor autorizado Bosch Rexroth, Aventics, Donaldson, Enerpac e Bonfiglioli, Realizamos Manutenção e Montagem de Equipamentos Industriais">
  <meta name="robots" content="index, follow">
  <meta name="keywords" content="Rotec, ExactControl, Automação, Anchor, Bosch Rexroth, Equipamentos, Industriais, Manutenção, Montagem">
  <meta name="author" content="Rafael Freitas">

  <link rel="icon" href="_imagens/_logos/exact.ico" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="_css/main.css">
  <link rel="stylesheet" type="text/css" href="_css/animate.css">
  <link rel="stylesheet" type="text/css" href="_css/bootstrap-social.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/5.11.2/css/font-awesome.min.css"-->

  <link rel="stylesheet" type="text/css" href="_plugins/owlcarousel/owl.carousel.min.css">
  <link rel="stylesheet" type="text/css" href="_plugins/owlcarousel/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="_plugins/bootstrapSelect/bootstrap-select.min.css">
  <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="_plugins/owlcarousel/owl.carousel.min.js"></script>
  <script src="_plugins/bootstrapSelect/bootstrap-select.min.js"></script>
  <script src="https://kit.fontawesome.com/72b309629b.js" crossorigin="anonymous"></script>
  <script async src="_js/picturefill.min.js"></script>
  <script async src="_js/main.js"></script>

  <script>


    function alterarProdutos() {
      var elems = document.getElementsByClassName('divOwlCarousel');
      for (var i=0;i<elems.length;i+=1){
        elems[i].style.display = 'none';
      }
      var idDiv = document.getElementById("selectProdutos").value;
      document.getElementById(idDiv).style.display = 'block';

    }

    $(document).ready(function(){
      if (window.navigator.userAgent.includes("Firefox")) {
        $(".carouselLogo04").css("max-width", "300px");
      }
    })

  </script>
  <style type="text/css">

    @media (max-width: 900px){
      .navbar-telefone{
        display: none;
      }
    }

    @media (min-width: 768px) {
      .navbar-telefone-mobile{
        display: none;
      }
    }

    @media (max-width: 768px) {
      .navbar-header{
        background-color: #31416b!important;
      }
    }

  </style>

</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">
  
  <div id="section0" class="container-fluid">
    <img src="">
    <div style="float: right; margin-right: 30px;">
      <img src="_imagens/_logos/logo_topo1.png" id="logo1" class="animated fadeInDown topoMobile"><br/>
      <img src="_imagens/_logos/auto.png" id="autoInicio" class="animated fadeInRight topoMobileAuto">
    </div>
  </div>

  <nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="198" style="background-color: #31416B;"><!--696969-->
    <div class="container-fluid" style="padding: 0px!important;">
      <div class="navbar-header" style="position: relative; cursor: pointer!important;" onclick="window.localtion.reload(true)">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" onclick="window.localtion.reload(true)" href="http://exactcontrol.com.br"><img class="logoMenu" src="_imagens/_logos/exactMenu1.png"></a>
        <div class="navbar-telefone-mobile" style="color: #FFF; position: absolute; right: 70px; top: 3px;" >
            <label>(81) 3471-6198</label><br>
            <label>(81) 99644-1160</label> <i class="fab fa-whatsapp"></i>
        </div>
        <!--div class="divlogoMenu"><img class="logoMenu" src="_imagens/_logos/exactMenu2.png"></div-->
      </div>
      <div>
        <div class="collapse navbar-collapse" id="myNavbar" style="position: relative;">
          <ul class="nav navbar-nav">
            <!--li><a href="#section1">Inicio</a></li-->
            <li><a href="#section3" style="color: #fff;">Produtos</a></li>
            <li><a href="#section4" style="color: #fff;">Serviços</a></li>
            <li><a href="#section5" style="color: #fff;">Fornecedores</a></li>
            <li><a href="#section2" style="color: #fff;">Quem Somos</a></li>
            <li><a href="#section6" style="color: #fff;">Localização</a></li>
            <li><a href="#section7" style="color: #fff;">Contato</a></li>
          </ul>
          <div class="navbar-telefone" style="color: #FFF; position: absolute; right: 30px; top: 3px;" >
            <label>(81) 3471-6198</label><br>
            <label>(81) 99644-1160</label> <i class="fab fa-whatsapp"></i>
          </div>
        </div>
      </div>
    </div>
  </nav>    

  <div id="section1" class="container-fluid">
    <div class="container">
      <div id="carouselInicio" class="carousel slide" data-ride="carousel">

        <!-- Wrapper for slides  bonficlionli -->
        <div class="carousel-inner">

          <div class="item active">
            <img src="_imagens/_carousel1/01.png">
            <div class="carousel-caption CaptionCarousel01">
              <img class="carouselLogo carouselLogo01 animated fadeInDown" src="_imagens/_catalogo/aventics2.png">
              <img class="carouselLogo carouselLogo02 animated fadeInDown" src="_imagens/_catalogo/donaldson2.png">
              <img class="carouselLogo carouselLogo04 animated fadeInDown" src="_imagens/_catalogo/bosh2.jpg" style="max-width: 413px;">
              <img class="carouselLogo carouselLogo05 animated fadeInDown" src="_imagens/_catalogo/bonficlionli.png">
              <img class="carouselLogo carouselLogo06 animated fadeInDown" src="_imagens/_catalogo/enerpac2.jpg">
            </div>
          </div><!-- End Item -->

          <div class="item">
            <img src="_imagens/_carousel1/hidraulica.png">
          </div><!-- End Item -->

          <div class="item">
            <img src="_imagens/_carousel1/filtros2.jpg">
            <div class="carousel-caption CaptionCarouselFiltro animated fadeInRight">
              <h3>Filtros Hidráulicos</h3>
              <br>
              <p class="captionPresenca">Uma inovadora tecnologia de filtro para um máximo desempenho hidráulico.</p>
              <img src="_imagens/_catalogo/bosh2.jpg" style="max-width: 200px;margin-right: 10px;-webkit-animation-delay: 1s;" class="animated fadeInUp">
            </div>
          </div><!-- End Item -->

          <div class="item">
            <img src="_imagens/_carousel1/acionamentos.jpg">
            <div class="carousel-caption CaptionCarouselFiltro animated fadeInRight">
              <h3>Acionamentos Elétricos e Controle</h3>
              <br>
              <p class="captionPresenca">Inversores de Frequência, Servo Acionamento, Servo Motores e PLCs</p>
              <img src="_imagens/_catalogo/bosh2.jpg" style="max-width: 200px;margin-right: 10px;-webkit-animation-delay: 1s;" class="animated fadeInUp">
            </div>
          </div><!-- End Item -->

          <div class="item">
            <img src="_imagens/_carousel1/linear.jpg">
            <div class="carousel-caption CaptionCarouselFiltro2 animated fadeInRight">
              <h3>Tecnologia Linear</h3>
              <br>
              <p class="captionPresenca">A Bosch Rexroth oferece um portfólio completo de guias lineares, eixos e rolamentos lineares.</p>
              <img src="_imagens/_catalogo/bosh2.jpg" style="max-width: 200px;margin-right: 10px;-webkit-animation-delay: 1s;" class="animated fadeInUp">
            </div>
          </div><!-- End Item -->

          <div class="item">
            <img src="_imagens/_carousel1/pneumatica.jpg">
          </div><!-- End Item -->

          <div class="item">
            <img src="_imagens/_carousel1/donaldson.jpg">
            <div class="carousel-caption CaptionCarouselFiltro animated fadeInDown">
              <h2>Filtros de Processo</h2>
              <br>
              <p class="captionPresenca">Somos Líderes em Soluções de Filtragem, uma empresa global com presença local.</p>
              <img src="_imagens/_catalogo/donaldson1.jpg" style="max-width: 200px;margin-right: 10px;-webkit-animation-delay: 1s;" class="animated fadeIn">
            </div>
          </div><!-- End Item -->

          <div class="item">
            <img src="_imagens/_carousel1/11.2.jpg">
            <div class="carousel-caption CaptionCarouselFiltro2 animated fadeInDown">
              <h2>Feramentas Hidráulicas</h2>
              <br>
              <p class="captionPresenca">Lider mundial em Ferramentas Hidráulicas e levantamentos pesados.</p>
              <img src="_imagens/_catalogo/enerpac.jpg" style="max-width: 200px;margin-right: 10px;-webkit-animation-delay: 1s;" class="animated fadeIn">
            </div>
          </div><!-- End Item -->
        </div><!-- End Carousel Inner -->

      <a class="left carousel-control" id="showLeft" href="#carouselInicio" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" id="showRight" href="#carouselInicio" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>


        <!-- <ul class="nav nav-pills nav-justified primeiroCarousel hiddeMobile">
         <li data-target="#carouselInicio" data-slide-to="0" class="active"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Linhas de Distribuição"><span class="glyphicon glyphicon-collapse-down"></span></a></li>
         <li data-target="#carouselInicio" data-slide-to="1"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Hidráulica Industrial"><span class="glyphicon glyphicon-collapse-down"></span></a></li>
         <li data-target="#carouselInicio" data-slide-to="2"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Filtros Hidraulicos"><span class="glyphicon glyphicon-collapse-down"></span></a></li>
         <li data-target="#carouselInicio" data-slide-to="3"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Acionamentos Elétricos e Controle"><span class="glyphicon glyphicon-collapse-down"></span></a></li>
         <li data-target="#carouselInicio" data-slide-to="4"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Tecnologia Linear"><span class="glyphicon glyphicon-collapse-down"></span></a></li>
         <li data-target="#carouselInicio" data-slide-to="5"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Pneumática"><span class="glyphicon glyphicon-collapse-down"></span></a></li>
         <li data-target="#carouselInicio" data-slide-to="6"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Filtros de Processo"><span class="glyphicon glyphicon-collapse-down"></span></a></li>
         <li data-target="#carouselInicio" data-slide-to="7"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Ferramentas Hidráulicas"><span class="glyphicon glyphicon-collapse-down"></span></a></li>
        </ul> -->
      </div><!-- End Carousel -->
    </div><!-- End Container -->
  </div> <!--Fim section 1-->

  <div id="section3" class="container-fluid">
    <div id="produtos_topo">
      <h1 class="text-center">Produtos</h1>
      <p class="text-center">Escolha a opção abaixo.</p>
      <div class="selecionarProdutos">
        <select class="selectpicker show-tick" id="selectProdutos" onchange="alterarProdutos()">
          <option value="produtos00">Hidráulica Industrial</option>
          <!--option value="produtos01">Hidráulica Mobil</option-->
          <option value="produtos02">Filtros Hidráulicos</option>
          <option value="produtos03">Acionamentos Elétricos e Controle</option>
          <option value="produtos04">Tecnologia Linear</option>
          <option value="produtos05">Pneumática</option>
          <!--option value="produtos06">Sensores</option-->
          <!--option value="produtos07">Mangueiras e Conexões</option-->
          

          <!-- <option value="produtos08">Acoplamentos</option> -->
          <option value="produtos09">Filtros de Processo</option>
          <option value="produtos10">Ferramentas Hidráulicas</option>
          <!-- <option value="produtos11">Correias</option> -->
          <option value="produtos12">Redutores</option>
        </select>
      </div><!--Fim selecionarProdutos 1-->
    </div><!--Fim produtos_topo 1-->

    <div id="pog">
      <div class="container divOwlCarousel animated fadeIn" id="produtos00">
        <div class="owl-carousel owl-theme" id="owl-produtos-01">
          <div class="item"><img src="_imagens/_owlcarousel/produtos00/01.jpg" class="img-thumbnail"><h4 class="text-center">Acumulador</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos00/02.jpg" class="img-thumbnail"><h4 class="text-center">Blocos</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos00/03.jpg" class="img-thumbnail"><h4 class="text-center">Bomba de Engrenagem</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos00/04.jpg" class="img-thumbnail"><h4 class="text-center">Bomba de Pistões A10VO</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos00/05.jpg" class="img-thumbnail"><h4 class="text-center">Bomba de Pistões PR4</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos00/06.jpg" class="img-thumbnail"><h4 class="text-center">Cilindros</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos00/07.jpg" class="img-thumbnail"><h4 class="text-center">Elemento Lógico</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos00/08.jpg" class="img-thumbnail"><h4 class="text-center">Sytronix</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos00/09.jpg" class="img-thumbnail"><h4 class="text-center">Unidades Hidráulicas</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos00/10.jpg" class="img-thumbnail"><h4 class="text-center">Válvula Direcional</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos00/11.jpg" class="img-thumbnail"><h4 class="text-center">Válvula Proporcional</h4></div>
        </div>
      </div><!-- / .container produtos00-->

      <div class="container divOwlCarousel animated fadeIn" id="produtos01">
        <div class="owl-carousel owl-theme" id="owl-produtos-01">
          <div class="item"><img src="_imagens/_owlcarousel/produtos01/01.jpg" class="img-thumbnail"><h4 class="text-center">Hidráulica Mobil</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos01/02.jpg" class="img-thumbnail"><h4 class="text-center">Motores Hidráulicos</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos01/03.jpg" class="img-thumbnail"><h4 class="text-center">Bombas Hidráulicas</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos01/04.jpg" class="img-thumbnail"><h4 class="text-center">Bombas Hidráulicas</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos01/05.jpg" class="img-thumbnail"><h4 class="text-center">Motores Hidráulicos</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos01/06.jpg" class="img-thumbnail"><h4 class="text-center">Comandos Hidráulicos</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos01/07.jpg" class="img-thumbnail"><h4 class="text-center">Bombas Hidráulicas</h4></div>
        </div>
      </div><!-- / .container produtos01-->

      <div class="container divOwlCarousel animated fadeIn" id="produtos02">
        <div class="owl-carousel owl-theme" id="owl-produtos-geral">
          <div class="item"><img src="_imagens/_owlcarousel/produtos02/01.jpg" class="img-thumbnail"><h4 class="text-center">Pure Power</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos02/02.jpg" class="img-thumbnail"><h4 class="text-center">Elementos</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos02/03.jpg" class="img-thumbnail"><h4 class="text-center">Elementos</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos02/04.jpg" class="img-thumbnail"><h4 class="text-center">Filtros</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos02/05.jpg" class="img-thumbnail"><h4 class="text-center">Filtros</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos02/06.jpg" class="img-thumbnail"><h4 class="text-center">SpinOn</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos02/07.jpg" class="img-thumbnail"><h4 class="text-center">Sistema de filtragem</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos02/08.jpg" class="img-thumbnail"><h4 class="text-center">Filtros</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos02/09.jpg" class="img-thumbnail"><h4 class="text-center">Sistema de filtragem</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos02/10.jpg" class="img-thumbnail"><h4 class="text-center">Filtros</h4></div>
        </div>
      </div><!-- / .container produtos02-->

      <div class="container divOwlCarousel animated fadeIn" id="produtos03">
        <div class="owl-carousel owl-theme" id="owl-produtos-geral">
          <div class="item"><img src="_imagens/_owlcarousel/produtos03/01.jpg" class="img-thumbnail"><h4 class="text-center">PLC</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos03/02.jpg" class="img-thumbnail"><h4 class="text-center">ServoMotor</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos03/03.jpg" class="img-thumbnail"><h4 class="text-center">Drives</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos03/04.jpg" class="img-thumbnail"><h4 class="text-center">Controles</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos03/05.jpg" class="img-thumbnail"><h4 class="text-center">Inversores de Frequência</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos03/06.jpg" class="img-thumbnail"><h4 class="text-center">Inversores de Frequência</h4></div>
        </div>
      </div><!-- / .container produtos03-->

      <div class="container divOwlCarousel animated fadeIn" id="produtos04">
        <div class="owl-carousel owl-theme" id="owl-produtos-geral">
          <div class="item"><img src="_imagens/_owlcarousel/produtos04/01.jpg" class="img-thumbnail"><h4 class="text-center">Tecnologia de Montagem</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos04/02.jpg" class="img-thumbnail"><h4 class="text-center">Tecnologia de Montagem</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos04/03.jpg" class="img-thumbnail"><h4 class="text-center">Tecnologia Linear</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos04/04.jpg" class="img-thumbnail"><h4 class="text-center">Tecnologia Linear</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos04/05.jpg" class="img-thumbnail"><h4 class="text-center">Varioflow</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos04/06.jpg" class="img-thumbnail"><h4 class="text-center">Mecatrônica</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos04/07.jpg" class="img-thumbnail"><h4 class="text-center">Transportadores</h4></div>
        </div>
      </div><!-- / .container produtos04-->

      <div class="container divOwlCarousel animated fadeIn" id="produtos05">
        <div class="owl-carousel owl-theme" id="owl-produtos-geral">
          <div class="item"><img src="_imagens/_owlcarousel/produtos05/01.jpg" class="img-thumbnail"><h4 class="text-center">Cilindros</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos05/02.jpg" class="img-thumbnail"><h4 class="text-center">Blocos</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos05/03.jpg" class="img-thumbnail"><h4 class="text-center">Cilindros</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos05/04.jpg" class="img-thumbnail"><h4 class="text-center">Conjunto de Preparação de Ar</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos05/05.jpg" class="img-thumbnail"><h4 class="text-center">Acessórios</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos05/06.jpg" class="img-thumbnail"><h4 class="text-center">Conjunto de Preparação de Ar</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos05/07.jpg" class="img-thumbnail"><h4 class="text-center">Pneumática</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos05/08.jpg" class="img-thumbnail"><h4 class="text-center">Valvulas 740</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos05/09.jpg" class="img-thumbnail"><h4 class="text-center">Blocos</h4></div>
        </div>
      </div><!-- / .container produtos05-->

      <div class="container divOwlCarousel animated fadeIn" id="produtos06">
        <div class="owl-carousel owl-theme" id="owl-produtos-geral">
          <div class="item"><img src="_imagens/_owlcarousel/produtos06/01.jpg" class="img-thumbnail"><h4 class="text-center">Tecnologia em Sensores</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos06/02.jpg" class="img-thumbnail"><h4 class="text-center">Tecnologia de conexão</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos06/03.jpg" class="img-thumbnail"><h4 class="text-center">RFID</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos06/04.jpg" class="img-thumbnail"><h4 class="text-center">Alimentação eficiente de tensão</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos06/05.jpg" class="img-thumbnail"><h4 class="text-center">Machine Vision e identificação ótica</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos06/06.jpg" class="img-thumbnail"><h4 class="text-center">Tecnologia de rede industrial</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos06/07.jpg" class="img-thumbnail"><h4 class="text-center">Dispositivos de aviso e indicação</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos06/08.jpg" class="img-thumbnail"><h4 class="text-center">Safety</h4></div>
        </div>
      </div><!-- / .container produtos06-->

      <div class="container divOwlCarousel animated fadeIn" id="produtos07">
        <div class="owl-carousel owl-theme" id="owl-produtos-geral">
          <div class="item"><img src="_imagens/_owlcarousel/produtos07/01.png" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos07/02.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos07/03.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos07/04.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos07/05.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos07/06.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos07/07.jpg" class="img-thumbnail"><h4 class="text-center"> </h4></div>
        </div>
      </div><!-- / .container produtos07-->

      <div class="container divOwlCarousel animated fadeIn" id="produtos08">
        <div class="owl-carousel owl-theme" id="owl-produtos-08">
          <div class="item"><img src="_imagens/_owlcarousel/produtos08/01.jpg" class="img-thumbnail"><h4 class="text-center">Thomas, Addax, Euroflex</br> e Moduflex</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos08/02.jpg" class="img-thumbnail"><h4 class="text-center">Rexnord Elastomeric Omega,</br>Viva or Wrapflex</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos08/03.jpg" class="img-thumbnail"><h4 class="text-center">Falk Steelflex Grid Couplings</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos08/04.jpg" class="img-thumbnail"><h4 class="text-center">Falk True Torque</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos08/05.jpg" class="img-thumbnail"><h4 class="text-center">Coupling Parts & Kits</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos08/06.jpg" class="img-thumbnail"><h4 class="text-center">Falk Lifelign Gear Couplings</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos08/07.jpg" class="img-thumbnail"><h4 class="text-center">Falk Orange Peel</h4></div>
        </div>
      </div><!-- / .container produtos08-->

      <div class="container divOwlCarousel animated fadeIn" id="produtos09">
        <div class="owl-carousel owl-theme" id="owl-produtos-09">
          <div class="item"><img src="_imagens/_owlcarousel/produtos09/01.jpg" class="img-thumbnail"><h4 class="text-center">Carcaças DF</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos09/02.jpg" class="img-thumbnail"><h4 class="text-center">Filtros de Três Etapas DF-T</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos09/03.jpg" class="img-thumbnail"><h4 class="text-center">Secadores Buran DC</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos09/04.jpg" class="img-thumbnail"><h4 class="text-center">Filtros LifeTec®</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos09/06.jpg" class="img-thumbnail"><h4 class="text-center">Filtros Coalescente Ultrair</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos09/07.jpg" class="img-thumbnail"><h4 class="text-center">Secadores Ultrapac 2000</h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos09/08.jpg" class="img-thumbnail"><h4 class="text-center">Carcaças P-EG</h4></div>
        </div>
      </div><!-- / .container produtos09-->

      <div class="container divOwlCarousel animated fadeIn" id="produtos10">
        <div class="owl-carousel owl-theme" id="owl-produtos-geral">
          <div class="item"><img src="_imagens/_owlcarousel/produtos10/01.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos10/02.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos10/03.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos10/04.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos10/05.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos10/06.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos10/07.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos10/08.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos10/09.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos10/10.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos10/11.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
        </div>
      </div><!-- / .container produtos10-->

      <div class="container divOwlCarousel animated fadeIn" id="produtos11">
        <div class="owl-carousel owl-theme" id="owl-produtos-geral">
          <div class="item"><img src="_imagens/_owlcarousel/produtos11/01.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos11/02.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos11/03.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos11/04.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos11/05.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos11/06.png" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos11/07.jpg" class="img-thumbnail"><h4 class="text-center"></h4></div>
        </div>
      </div><!-- / .container produtos11-->

      <div class="container divOwlCarousel animated fadeIn" id="produtos12">
        <div class="owl-carousel owl-theme" id="owl-produtos-geral">
          <div class="item"><img src="_imagens/_owlcarousel/produtos12/01.png" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos12/02.png" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos12/03.png" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos12/04.png" class="img-thumbnail"><h4 class="text-center"></h4></div>
          <div class="item"><img src="_imagens/_owlcarousel/produtos12/05.png" class="img-thumbnail"><h4 class="text-center"></h4></div>
        </div>
      </div><!-- / .container produtos11-->
    </div> <!-- Pog -->

    <div class="container">
      
      <p id="descricao-indrodutoria2" align="justify" class="recuo">A empresa entende que o melhor caminho para um alto rendimento está em reduzir o fator desperdício do processo de automação, projetando equipamentos com inteligência e buscando continuamente a melhoria do sistema de gestão da qualidade.<br>
      </p>
      <p id="descricao-indrodutoria2" align="justify" class="recuo" style="margin-bottom: 50px;">Para mais informações sobre os produtos que fornecemos, não hesite, entre em contato conosco. Ou  caso prefira, poderá consultar os detalhes sobre os produtos em seus respectivos catálogos. Abaixo nós disponibilizamos um redirecionamento para você. Basta escolher as opções, e ao clicar no botão uma aba será aberta e você será redirecionado.</p>
      
      <div id="catalogo">
        <div id="catalogoSegundaOpcao" style="margin-bottom: 30px;">

          <div class="row"> 
            <div class="col-md-6 col-xs-12">
              <ul class="nav nav-pills nav-stacked">
                <li class="active"><a data-toggle="pill" href="#home">Hidráulica Industrial</a></li>
                <!--li><a data-toggle="pill" href="#opcao0">Hidráulica Mobil</a></li-->
                <li><a data-toggle="pill" href="#opcao1">Filtros Hidráulicos</a></li>
                <li><a data-toggle="pill" href="#opcao2">Acionamentos Elétricos e Controle</a></li>
                <li><a data-toggle="pill" href="#opcao3">Tecnologia Linear</a></li>
                <li><a data-toggle="pill" href="#opcao4">Pneumática</a></li>
                <!-- <li><a data-toggle="pill" href="#opcao7">Acoplamentos</a></li> -->
                <li><a data-toggle="pill" href="#opcao8">Filtros de Processo</a></li>
                <li><a data-toggle="pill" href="#opcao9">Ferramentas Hidráulicas</a></li>
                <!--li><a data-toggle="pill" href="#opcao10">Correias</a></li-->
                <li><a data-toggle="pill" href="#opcao5">Redutores</a></li>

              </ul>
            </div>  <!-- Col-->

            <div class="col-md-6 col-xs-12">
              <div class="tab-content">

                <div id="home" class="tab-pane fade in active">
                  <p class="text-center"><img style="max-width: 350px" src="_imagens/_catalogo/bosh2.jpg"></p>
                  <div style="margin: auto; max-width: 400px;">
                    <h3>Hidráulica Industrial</h3>
                    <p>A empresa reúne as experiências mundiais das aplicações nos segmentos de mercado Aplicações Mobil, Máquinas e Engenharia e Automação Fabril.</p>
                    <p>Aqui você vai encontrar:</p>
                    <ul>
                      <li>Bombas</li>
                      <li>Motores</li>
                      <li>Cilindros</li>
                      <li>Catálogo de Produtos</li>
                      <a target="_blank" href="https://www.boschrexroth.com/pt/br/produtos/grupos-de-produtos/hidraulica-industrial/hidraulica-industrial" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                    </ul>
                  </div>
                </div>

                <div id="opcao0" class="tab-pane fade">
                  <p class="text-center"><img style="max-width: 350px" src="_imagens/_catalogo/bosh2.jpg"></p>
                  <div style="margin: auto; max-width: 400px;">
                    <h3>Hidráulica Mobil</h3>
                    <p>Aqui você vai encontrar:</p>
                    <ul>
                      <li>Catálogo de Produtos</li>
                      <li>Sistemas e Módulos Funcionais</li>
                      <li>Destaques</li>
                      <a target="_blank" href="https://www.boschrexroth.com/pt/br/produtos/grupos-de-produtos/hidraulica-mobil/menu" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                    </ul>
                  </div>
                </div>

                <div id="opcao1" class="tab-pane fade">
                  <p class="text-center"><img style="max-width: 350px" src="_imagens/_catalogo/bosh2.jpg"></p>
                  <div style="margin: auto; max-width: 400px;">
                    <h3>Filtros Hidráulicos</h3>
                    <p>A Bosch Rexroth oferece um portfólio completo, incluindo filtros e elementos filtrantes com a mais recente tecnologia para o mais alto desempenho.</p>
                    <p>Aqui você vai encontrar:</p>
                    <ul>
                      <li>Dimensionamento e conversão</li>
                      <li>Diretório de Catálogos</li>
                      <li>Ficha Técnica</li>
                      <a target="_blank" href="https://www.boschrexroth.com/pt/br/produtos/grupos-de-produtos/sistemas-de-filtragem/menu" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                    </ul>
                  </div>
                </div>


                <div id="opcao2" class="tab-pane fade">
                  <p class="text-center"><img style="max-width: 350px" src="_imagens/_catalogo/bosh2.jpg"></p>
                  <div style="margin: auto; max-width: 400px;">
                    <h3>Acionamentos Elétricos e Controles</h3>
                    <p>Aqui você vai encontrar:</p>
                    <ul>
                      <li>Inversores de Frequência</li>
                      <li>Servo Acionamento</li>
                      <li>Servo Motores e Redutores</li>
                      <li>CPLs</li>
                      <a target="_blank" href="https://www.boschrexroth.com/pt/br/produtos/grupos-de-produtos/acionamentos-eletricos-e-controles/menu" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                    </ul>
                  </div>
                </div>


                <div id="opcao3" class="tab-pane fade">
                  <p class="text-center"><img style="max-width: 350px" src="_imagens/_catalogo/bosh2.jpg"></p>
                  <div style="margin: auto; max-width: 400px;">
                    <h3>Tecnologia Linear</h3>
                    <p>A Bosch Rexroth oferece um portfólio completo, incluindo guias lineares, eixos e rolamentos lineares, fusos, módulos lineares, sistemas cartesianos e esferas transferidoras e anéis de tolerância.</p>
                    <p>Aqui você vai encontrar:</p>
                    <ul>
                      <li>Cilindro EMC-HD</li>
                      <li>Segurança em Máquinas - Soluções Rexroth</li>
                      <li>SafeLogic Compact</li>
                      <li>Easy Handling</li>
                      <a target="_blank" href="https://www.boschrexroth.com/pt/br/produtos/grupos-de-produtos/tecnologia-de-acionamento-linear/menu" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                    </ul>
                  </div>
                </div>


                <div id="opcao4" class="tab-pane fade">
                  <p class="text-center"><img src="_imagens/_catalogo/aventics.png"></p>
                  <div style="margin: auto; max-width: 400px;">
                    <h3>Pneumática</h3>
                    <p>Aqui você vai encontrar:</p>
                    <ul>
                      <li>Catálogo completo com:</li>
                      <ul>
                        <li>Cilindros</li>
                        <li>Acionamentos</li>
                        <li>Acessórios</li>  
                        <li>Conexões</li>
                      </ul>
                      <a target="_blank" href="http://www.aventics.com/pneumatics-catalog/Vornavigation/VorNavi.cfm?Language=PT&Variant=internet&PageID=g53567" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                    </ul>
                  </div>
                </div>

                <div id="opcao5" class="tab-pane fade">
                  <p class="text-center"><img style="max-width: 412px;" src="_imagens/_catalogo/bonficlionli.png"></p>
                  <div style="margin: auto; max-width: 400px;">
                    <h3>Redutores</h3>
                    <p>Aqui você vai encontrar:</p>
                    <ul>
                      <li>Detalhe sobre:</li>
                      <ul>
                        <li>Produtos</li>
                        <li>Soluções</li>
                        <li>Mercados e aplicações</li>  
                      </ul>
                      <a target="_blank" href="https://www.bonfiglioli.com/brazil/pt" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                    </ul>
                  </div>
                </div>


                <div id="opcao6" class="tab-pane fade">
                  <p class="text-center"><img src="_imagens/_catalogo/manuli.png" style="margin-top: 30px;"></p>
                  <div style="margin: auto; max-width: 400px;">
                    <h3>Mangueiras e Conexões</h3>
                    <p>São especificamente projetadas para sistemas hidráulicos e equipamentos em diversos setores, tais como máquinas de construção, mineração, processos industriais, agricultura, equipamentos de construção rodoviária, perfuração e muitas outras.</p>
                    <p>Aqui você vai encontrar:</p>
                    <ul>
                      <li>Catálogo completo</li>
                      <a target="_blank" href="http://www.manuli-hydraulics.com/products-documents.asp?IDProdotto=1&IDDivisione=1" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                    </ul>
                  </div>
                </div>


                <div id="opcao7" class="tab-pane fade">
                  <p class="text-center"><img src="_imagens/_catalogo/rexnord.png" style="margin-top: 30px;"></p>
                  <div style="margin: auto; max-width: 400px;">
                    <h3>Acoplamentos</h3>
                    <p>Quando o assunto é fornecer produtos de engenharia altamente especializados que aumentem a produtividade e eficiência no mundo, a Rexnord é a mais confiável no mercado.</p>
                    <p>Aqui você vai encontrar:</p>
                    <ul>
                      <li><a target="_blank" href="http://www.rexnord.com.br/catproduto/discos/">Discos</a></li>
                      <li><a target="_blank" href="http://www.rexnord.com.br/catproduto/elastomericos/">Elastoméricos</a></li>
                      <li><a target="_blank" href="http://www.rexnord.com.br/catproduto/composite/">Composite</a></li>
                      <li><a target="_blank" href="http://www.rexnord.com.br/catproduto/engrenagens/">Engrenagens</a></li>
                      <li><a target="_blank" href="http://www.rexnord.com.br/catproduto/lubrificados/">Lubrificados</a></li>
                      <li><a target="_blank" href="http://www.rexnord.com.br/catproduto/controle-de-torque/">Controle de torque</a></li>
                    </ul>
                  </div>
                </div>


                <div id="opcao8" class="tab-pane fade">
                  <p class="text-center"><img src="_imagens/_catalogo/donaldson.png"></p>
                  <div style="margin: auto; max-width: 400px;">
                    <h3>Filtros de Processo</h3>
                    <p>Aqui você vai encontrar:</p>
                    <ul>
                      <li>Catálogos</li>
                      <li>Novidades</li>
                      <a href="http://www.donaldsonfiltros.com.br/products.html" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                    </ul>
                  </div>
                </div>


                <div id="opcao9" class="tab-pane fade">
                  <p class="text-center"><img src="_imagens/_catalogo/enerpac.jpg"></p>
                  <div style="margin: auto; max-width: 400px;">
                    <h3>Ferramentas Hidráulicas</h3>
                    <p>Lider mundial em Ferramentas Hidráulicas. Equipamentos e soluções para levantamento pesado.</p>
                    <p>Aqui você vai encontrar:</p>
                    <ul>
                      <li>Catálogos</li>
                      <li>Manuais de Instrução</li>
                      <a target="_blank" href="http://www.enerpac.com/pt/downloads" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                    </ul>
                  </div>
                </div>


                <div id="opcao10" class="tab-pane fade">
                  <p class="text-center"><img src="_imagens/_catalogo/fenner-drives.png"></p>
                  <div style="margin: auto; max-width: 400px;">
                    <h3>Correias</h3>
                    <p>Aqui você vai encontrar:</p>
                    <ul>
                      <li>Catálogos</li>
                      <li>Perguntas Frequentes</li>
                      <li>Folhetos</li>
                      <a target="_blank" href="http://www.fennerdrives.com/productliterature/" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                    </ul>
                  </div>
                </div>

              </div><!-- tab-content-->
            </div> <!-- Col-->
          </div> <!-- Row-->

        </div> <!-- catalogoSegundaOpcao-->
      </div><!-- DIV Catalogo-->

    </div><!-- DIV Container-->

    <div class="hiddeDesktop" id="hiddeDesktop" style="max-width: 450px; margin-right: auto;margin-left: auto;">
      <div class="row">
        <div class="col-sm-12">
          <div class="panel-group" id="accordion">

            <div class="panel panel-default">
              <div class="panel-heading" >
                <h3 class="panel-title" >
                  <a data-toggle="collapse" data-parent="#accordion" href= #collapse1>Hidráulica Industrial</a>
                </h3>
              </div>
              <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                  <p class="text-center"><img style="max-width: 300px" src="_imagens/_catalogo/bosh2.jpg"></p>
                  <div style="margin: auto; max-width: 400px;">
                    <h3>Hidráulica Industrial</h3>
                    <p>A empresa reúne as experiências mundiais das aplicações nos segmentos de mercado Aplicações Mobil, Máquinas e Engenharia e Automação Fabril.</p>
                    <p>Aqui você vai encontrar:</p>
                    <ul>
                      <li>Bombas</li>
                      <li>Motores</li>
                      <li>Cilindros</li>
                      <li>Catálogo de Produtos</li>
                      <a target="_blank" href="https://www.boschrexroth.com/pt/br/produtos/grupos-de-produtos/hidraulica-industrial/hidraulica-industrial" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
              
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                  Filtros Hidráulicos</a>
                </h3>
              </div>
              <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="text-center"><img style="max-width: 300px" src="_imagens/_catalogo/bosh2.jpg"></p>
                      <div style="margin: auto; max-width: 400px;">
                        <h3>Filtros Hidráulicos</h3>
                        <p>A Bosch Rexroth oferece um portfólio completo, incluindo filtros e elementos filtrantes com a mais recente tecnologia para o mais alto desempenho.</p>
                        <p>Aqui você vai encontrar:</p>
                        <ul>
                          <li>Dimensionamento e conversão</li>
                          <li>Diretório de Catálogos</li>
                          <li>Ficha Técnica</li>
                          <a target="_blank" href="https://www.boschrexroth.com/pt/br/produtos/grupos-de-produtos/sistemas-de-filtragem/menu" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                        </ul>
                      </div>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading" >
                <h3 class="panel-title" >
                  <a data-toggle="collapse" data-parent="#accordion" href= #collapse4>Acionamentos Elétricos e Controle</a>
                </h3>
              </div>
              <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="text-center"><img style="max-width: 300px" src="_imagens/_catalogo/bosh2.jpg"></p>
                      <div style="margin: auto; max-width: 400px;">
                        <h3>Acionamentos Elétricos e Controles</h3>
                        <p>Aqui você vai encontrar:</p>
                        <ul>
                          <li>Inversores de Frequência</li>
                          <li>Servo Acionamento</li>
                          <li>Servo Motores e Redutores</li>
                          <li>CPLs</li>
                          <a target="_blank" href="https://www.boschrexroth.com/pt/br/produtos/grupos-de-produtos/acionamentos-eletricos-e-controles/menu" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                        </ul>
                      </div>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading" >
                <h3 class="panel-title" >
                  <a data-toggle="collapse" data-parent="#accordion" href= #collapse5>Tecnologia Linear</a>
                </h3>
              </div>
              <div id="collapse5" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="text-center"><img style="max-width: 300px" src="_imagens/_catalogo/bosh2.jpg"></p>
                      <div style="margin: auto; max-width: 400px;">
                        <h3>Tecnologia Linear</h3>
                        <p>A Bosch Rexroth oferece um portfólio completo, incluindo guias lineares, eixos e rolamentos lineares, fusos, módulos lineares, sistemas cartesianos e esferas transferidoras e anéis de tolerância.</p>
                        <p>Aqui você vai encontrar:</p>
                        <ul>
                          <li>Cilindro EMC-HD</li>
                          <li>Segurança em Máquinas - Soluções Rexroth</li>
                          <li>SafeLogic Compact</li>
                          <li>Easy Handling</li>
                          <a target="_blank" href="https://www.boschrexroth.com/pt/br/produtos/grupos-de-produtos/tecnologia-de-acionamento-linear/menu" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                        </ul>
                      </div>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading" >
                <h3 class="panel-title" >
                  <a data-toggle="collapse" data-parent="#accordion" href= #collapse6>Pneumática</a>
                </h3>
              </div>
              <div id="collapse6" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="text-center"><img src="_imagens/_catalogo/aventics.png"></p>
                      <div style="margin: auto; max-width: 400px;">
                        <h3>Pneumática</h3>
                        <p>Aqui você vai encontrar:</p>
                        <ul>
                          <li>Catálogo completo com:</li>
                          <ul>
                            <li>Cilindros</li>
                            <li>Acionamentos</li>
                            <li>Acessórios</li>  
                            <li>Conexões</li>
                          </ul>
                          <a target="_blank" href="http://www.aventics.com/pneumatics-catalog/Vornavigation/VorNavi.cfm?Language=PT&Variant=internet&PageID=g53567" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                        </ul>
                      </div>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading" >
                <h3 class="panel-title" >
                  <a data-toggle="collapse" data-parent="#accordion" href= #collapse10>Filtros de Processo</a>
                </h3>
              </div>
              <div id="collapse10" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="text-center"><img src="_imagens/_catalogo/donaldson.png"></p>
                      <div style="margin: auto; max-width: 400px;">
                        <h3>Filtros de Processo</h3>
                        <p>Aqui você vai encontrar:</p>
                        <ul>
                          <li>Catálogos</li>
                          <li>Novidades</li>
                          <a href="http://www.donaldsonfiltros.com.br/products.html" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                        </ul>
                      </div>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading" >
                <h3 class="panel-title" >
                  <a data-toggle="collapse" data-parent="#accordion" href= #collapse11>Ferramentas Hidráulicas</a>
                </h3>
              </div>
              <div id="collapse11" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="text-center"><img src="_imagens/_catalogo/enerpac.jpg"></p>
                      <div style="margin: auto; max-width: 400px;">
                        <h3>Ferramentas Hidráulicas</h3>
                        <p>Lider mundial em Ferramentas Hidráulicas. Equipamentos e soluções para levantamento pesado.</p>
                        <p>Aqui você vai encontrar:</p>
                        <ul>
                          <li>Catálogos</li>
                          <li>Manuais de Instrução</li>
                          <a target="_blank" href="http://www.enerpac.com/pt/downloads" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                        </ul>
                      </div>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading" >
                <h3 class="panel-title" >
                  <a data-toggle="collapse" data-parent="#accordion" href= #collapse12>Redutores</a>
                </h3>
              </div>
              <div id="collapse12" class="panel-collapse collapse">
                <div class="panel-body">
                  <p class="text-center"><img style="max-width: 300px" src="_imagens/_catalogo/bonficlionli.png"></p>
                      <div style="margin: auto; max-width: 400px;">
                        <h3>Redutores</h3>
                        <p>Aqui você vai encontrar:</p>
                        <ul>
                          <li>Detalhe sobre:</li>
                          <ul>
                            <li>Produtos</li>
                            <li>Soluções</li>
                            <li>Mercados e aplicações</li>  
                          </ul>
                          <a target="_blank" href="https://www.bonfiglioli.com/brazil/pt" class="btn btn-default" role="button" style="margin-top: 10px;">Acesse</a>
                        </ul>
                      </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

  </div> <!--section 3-->

  <div class="container-fluid" style="padding: 0px; height: 8px">
    <img src="_imagens/linha_divisoria.jpg" style="width: 100%;">
  </div>

  <div id="section4" class="container-fluid">
    <div id="servicos_topo">
      <h2 class="text-center">Serviços somente com a ExactControl</h2>
      <p class="text-center">Atuamos em todo o setor industrial, com mão-de-obra especializada e atendimento individualizado para análise, manutenção e execução de serviços.</p>

      <p class="text-center">
        <img class="imgServicos" sizes="(min-width: 1em) 72vw"
        srcset="_imagens/_services/servicos_pq.jpg 300w,
        _imagens/_services/servicos_md.jpg 592w,
        _imagens/_services/servicos_gr.jpg 792w"
        alt="…">
      </p>
    </div><!--servicos_topo-->
    <script>
      jQuery(function(){
        jQuery('#botao_collapse').click();
      });
    </script>
    
    <div class="container servicosPrincipal">
      <p class="text-center" style="margin-bottom: 10px;">
        <button type="button" id="botao_collapse" class="btn btn-primary demo1" data-toggle="collapse" data-target="#demo1" style="margin-top: 5px;">
          <span class="glyphicon glyphicon-plus"></span> Manutenção
        </button>
        <button type="button" class="btn btn-primary demo2" data-toggle="collapse" data-target="#demo2" style="margin-top: 5px;">
          <span class="glyphicon glyphicon-plus"></span> Instalações
        </button>
        <button type="button" class="btn btn-primary demo3" data-toggle="collapse" data-target="#demo3" style="margin-top: 5px;">
          <span class="glyphicon glyphicon-plus"></span> Análises
        </button>
        <button type="button" class="btn btn-primary demo4" data-toggle="collapse" data-target="#demo4" style="margin-top: 5px;">
          <span class="glyphicon glyphicon-plus"></span> Service EDC
        </button>
      </p>
      <div id="demo1" class="collapse">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <img src="_imagens/_services/manutencao.jpg" class="img-thumbnail" alt="Cinque Terre">     
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <h4 class="text-center">Manutenção de Componentes</h4>
            <p id="pservicos" align="justify">A parceria com as marcas representadas garante ampla capacidade de atender com rapidez e eficiência os clientes, assegurando seu processo produtivo através de um estoque variado de peças. Nossa manutenção conta com uma ampla qualidade, que vai desde as peças utilizadas para a mão-de-obra e a pós manutenção, onde você percebe que o resultado final vale a pena.</p>    
          </div>
        </div>
      </div>

      <div id="demo2" class="collapse">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <h4 class="text-center">Instalações de Equipamentos</h4>
            <p id="pservicos" align="justify">Realizaremos as instalações de seus equipamentos através de nossa experiência, de acordo com as suas necessidades individuais. Oferecendo assim, melhor eficiência, segurança e desempenho.</p>    
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <p class="text-center"><img src="_imagens/_services/instalacoes.jpg" class="img-thumbnail" alt="Cinque Terre" style="max-width: 317px;"></p>
          </div>
        </div>
      </div>

      <div id="demo3" class="collapse">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <p class="text-center"><img id="imgAnalises" src="_imagens/_services/analises.jpg" class="img-thumbnail" alt="Cinque Terre" style="max-width: 420px;"></p>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <h4 class="text-center">Análises em Sistemas Hidráulicos</h4>
            <p id="pservicos" align="justify">O monitoramento de condições indica o desgaste e relata a necessidade de substituição quando os limites críticos são alcançados. A reposição programada e o tempo de uso reduz o risco de paralisações da máquina e, portanto, aumenta a disponibilidade da máquina. Ao mesmo tempo, esta abordagem reduz os custos de manutenção, uma vez que apenas os componentes defeituosos ​​são substituídos.</p>       
          </div>
        </div>
      </div>

      <div id="demo4" class="collapse">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <h4 class="text-center">Service EDC</h4>
            <p id="pservicos" align="justify">Service para acionamentos elétricos e controles. Service para tecnologia de acionamento linear. Tecnologia de parafusamento e service para montagem.</p>    
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <p class="text-center"><img id="imgAnalises" src="_imagens/_services/service_edc.jpg" class="img-thumbnail" alt="Cinque Terre" style="max-width: 500px;"></p>
          </div>
        </div>
      </div> 
    </div><!--servicosPrincipal-->
  </div> <!--section 4-->

  <div class="container-fluid" style="padding: 0px; height: 8px">
    <img src="_imagens/linha_divisoria.jpg" style="width: 100%;">
  </div>

  <div id="section5" class="container-fluid">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <!--li data-target="#myCarousel" data-slide-to="2"></li-->
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <!-- <li data-target="#myCarousel" data-slide-to="3"></li> -->
        <!--li data-target="#myCarousel" data-slide-to="5"></li-->
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">

        <div class="item active">
          <img class="slide_carousel" src="_imagens/_carousel/bosch_rexroth.png" alt="First slide">
          <div class="carousel-caption hide-on-mobile">
            <h1 class="slide_carousel animated bounceInDown">Distribuidor Autorizado</h1>
            <p class="slide_carousel animated bounceInRight" style="margin-bottom: 30px!important; color: #fff!important;">Parceiros comprometidos com a qualidade.</p>
          </div>
        </div>

        <div class="item">
          <img class="slide_carousel animated fadeIn" src="_imagens/_carousel/aventics.jpg" alt="Second slide">
          <div class="carousel-caption hide-on-mobile" style="top:30px!important;">
            <h2 class="slide_carousel2 animated fadeIn" style="color: #fff">Grande experiência no mercado de<br>Pneumática Industrial</h2>
          </div>
          <div class="carousel-caption hide-on-mobile" style="left:150px!important;margin-bottom: 280px;right: auto;">
            <img class="slide_carousel animated fadeIn" src="_imagens/_carousel/aventics.png" alt="Second slide">
          </div>
        </div>

        <!--div class="item">
          <img class="third-slide" src="_imagens/_carousel/balluff.jpg" alt="Third slide">
          <div class="carousel-caption hide-on-mobile">
            <h1 class="slide_carousel animated fadeInDown" style="color: #000; margin-bottom: 40px!important;">Alta qualidade em sensores.</h1>
          </div>
        </div-->

        <div class="item">
          <img class="Fourth slide" src="_imagens/_carousel/donaldson.jpg" alt="Fourth slide">
          <div class="carousel-caption hide-on-mobile" style="margin-bottom: 80px; color: #fff">
            <h1 class="slide_carousel animated zoomIn" style="margin-bottom: 15px!important">Líderes em Soluções de Filtragem.</h1>
          </div>
        </div>

        <!-- <div class="item">
          <img class="Fifth slide" src="_imagens/_carousel/rexnord.jpg" alt="Fifth slide">
          <div class="carousel-caption hide-on-mobile" style="margin-bottom: 40px; color: #fff">
            <h2 class="slide_carousel animated fadeInLeftBig">Mais de 35 Anos no Brasil.</h2>
          </div>
        </div> -->

        <!--div class="item">
          <img class="Sixth slide" src="_imagens/_carousel/manuli.jpg" alt="Sixth slide">
          <div class="carousel-caption hide-on-mobile" style="left:50px!important;margin-bottom: 80px;right: auto; color: ">
            <h2 class="slide_carousel animated fadeInLeftBig">Projetadas para</br>sistemas hidráulicos.</h2>
          </div>
          <div class="carousel-caption hide-on-mobile" style="left:150px!important;top: 80px;right: auto;">
            <img class="slide_carousel2 animated fadeInLeft" src="_imagens/_carousel/manuli_logo.png" alt="Sixth slide">
          </div>
          <div class="carousel-caption hide-on-mobile" style="left:10px!important;top: 20px;right: auto;">
            <img class="slide_carousel animated fadeInDown" src="_imagens/_carousel/manuli.png" alt="Sixth slide">
          </div>
        </div-->

        <div class="item">
          <img class="Eighth slide" src="_imagens/_carousel/enerpac.jpg" alt="Eighth slide">
          <div class="carousel-caption hide-on-mobile">
            <h1 class="slide_carousel animated fadeInDownBig" style="margin-bottom: 40px!important">ENERPAC - Ferramentas Industriais</h1>
          </div>
        </div>

        <div class="item">
          <img class="Ninth slide" src="_imagens/_carousel/bonficlionli.jpg" alt="Ninth slide">
          <div class="carousel-caption hide-on-mobile" style="margin-bottom: 40px; color: #014584">
            <!--h2 class="slide_carousel animated fadeInLeftBig">Elimine o impacto da formação<br>de eletrostática.</h2-->
          </div>
        </div>

      </div><!-- carouselinner -->

      <!-- Controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
    </div><!--myCarousel 5-->
  </div> <!--section 5-->

  <div class="container-fluid" style="padding: 0px; height: 8px">
    <img src="_imagens/linha_divisoria.jpg" style="width: 100%;">
  </div>

  <div id="section2" class="container-fluid fundo_cinza">
    <div class="container" style="margin-bottom: 50px; margin-top: 120px;">
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
          <span class="glyphicon glyphicon-globe icone-maior esconderParaDeslizar"></span></br></br></br></br>
          <span class="glyphicon glyphicon-cog icone-maior esconderParaDeslizar esconderMobile"></span>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
          <h2 class="nossosValores">Nossos Projetos</h2>
          <p>A ExactControl possui uma equipe técnica especializada e apta a oferecer os mais variados tipos de soluções integradas para o segmento de automação industrial, atendendo às necessidades de cada cliente através de projetos personalizados. Contamos com uma abrangente linha de produtos com reconhecido Know-how em diversas aplicações.</p><br>

          <h2 class="nossosValores">Assistência Técnica</h2>
          <p>Contamos com excelente estrutura física e técnica, realizando serviços das mais variadas complexidades. Atuamos fortemente nos segmentos de mineração, siderurgia, papel e celulose, off shore, alimentos, entre outros.<br>A ExactControl é centro de serviços homologado das marcas Bosch Rexroth e Enerpac, contando com corpo técnico devidamente treinado e qualificado junto às fabricas.</p><br>

          <h2 class="nossosValores">Competitividade</h2>
          <p>Atender às expectativas e necessidades do mercado requer conhecimento e constante atualização. O cliente está cada vez mais exigente em relação à qualidade e preço.<br/>Ciente disso, a ExactControl vem investindo fortemente na busca de novos produtos que se enquadrem nessa nova realidade, importando diretamente de grandes fabricantes e conseguindo, assim, oferecer produtos de altíssima qualidade com preços competitivos.</p><br>
        </div>
      </div><!--row 2-->
    </div><!--container 2-->
  </div> <!--section 2-->

  <div id="section6" class="container-fluid map-container" style="padding: 0px;">
    <div id="localizacao_topo">
      <h2 style="color: #000; text-align: center; padding: 0; margin: 0;">Onde estamos</h2>
      <div id="endereco">
        <p align="center">Rua Srg. Silvino de Macedo Nº 208</p>
        <p align="center">Imbiribeira - Recife - PE</p>
        <p align="center">CEP: 51160-060 - (81) 3471-6198</p><br/>
        <!--p align="center">exactcontrol@exactcontrol.com.br</p-->
      </div><!--endereco-->
      <iframe class="maps" id="iframeMaps" src="https://maps.google.com/maps?width=700&amp;height=440&amp;hl=en&amp;q=exact%20control+(T%C3%ADtulo)&amp;ie=UTF8&amp;t=&amp;z=18&amp;iwloc=B&amp;output=embed" height="450" style="border:2px solid lightgrey;" allowfullscreen></iframe>
    </div><!--localizacao_topo-->
  </div><!--section6-->


  <div id="section7" class="container-fluid">
    <div id="formulario">
      <h2 style="margin-left: 25px;">Fale Conosco</h2>
      <p style="margin-left: 25px;">Caso deseje entrar em contato, preencha o formulário abaixo.</p>
      <form id="formularioContato" class="form-horizontal" style="max-width: 800px;">
        <fieldset id="fieldsetForm">
          <div class="form-group">
            <label class="control-label labelForm col-sm-2" for="nome">Nome:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="nome" name="nome" placeholder="Informe seu nome" required>
              <span class='glyphicon glyphicon-ok form-control-feedback imputForm'></span>
            </div><!--col-sm-10-->
          </div><!--form-group-->
          <div class="form-group">
            <label class="control-label labelForm col-sm-2" for="empresa">Empresa:</label>
            <div class="col-sm-10"> 
              <input type="text" class="form-control" id="empresa" name="empresa" placeholder="Qual empresa você representa?" required>
              <span class='glyphicon glyphicon-ok form-control-feedback imputForm'></span>
            </div><!--col-sm-10-->
          </div><!--form-group-->
          <div class="form-group">
            <label class="control-label labelForm col-sm-2" for="telefone">Telefone:</label>
            <div class="col-sm-10">
              <input pattern="^\d{2}-\d{5}-\d{4}$" type="tel" class="form-control" rows="3" id="telefone" name="telefone" OnKeyPress="formatar('##-#####-####', this)" maxlength="13" placeholder="00-00000-0000" style="max-width: 200px!important; margin-right: 0px!important;" required><span class='glyphicon glyphicon-ok form-control-feedback imputForm'></span>Caso deseje informar um Nº Fixo, colocar um 0 no lugar do 9º dígito.
            </div><!--col-sm-10-->
          </div><!--form-group-->
          <div class="form-group">
            <label class="control-label labelForm col-sm-2" for="email">E-mail:</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" id="email" name="email" placeholder="E-mail para contato." pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="exemplo@exemplo.com" required><span class='glyphicon glyphicon-ok form-control-feedback imputForm'></span>
            </div><!--col-sm-10-->
          </div><!--form-group-->
          <div class="form-group">
            <label class="control-label labelForm col-sm-2" for="comentario">Descrição:</label>
            <div class="col-sm-10">
              <textarea class="form-control" rows="3" id="comentario" name="comentario" placeholder="Descreva sua necessidade:" required></textarea><span class='glyphicon glyphicon-ok form-control-feedback imputForm'></span>
            </div><!--col-sm-10-->
          </div><!--form-group-->
          <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-default">Enviar</button>
              <img  id="loadGif" src="_imagens/_gif/load.gif" style="max-width: 50px;">
              <p class="text-center animated bounceInRight" id="sucessoEnvio"><span class="glyphicon glyphicon-ok"></span><strong> Recebemos seu contato, agora é so aguardar que retornaremos.</strong></p>
              <p class="text-center" id="erroBanco"></p>
              <p class="text-center" id="erroEnvio"></p>
            </div><!--col-sm-10-->
          </div><!--form-group-->
        </fieldset><!--fieldsetForm-->
      </form><!--formularioContato-->
    </div><!--formulario-->
  </div><!--section7-->

  <div class="container-fluid" style="padding: 0px; height: 8px">
    <img src="_imagens/linha_divisoria.jpg" style="width: 100%;">
  </div>

  <div id="section8" class="container-fluid">
    <img class="nodesRodape" src="_imagens/nodes1.png">
    <div id="footer">

      <div class="redTop">
        <a href="#section0" title="Clique para subir">
          <span class="glyphicon glyphicon-chevron-up"></span>
        </a>
        <img src="_imagens/_logos/rodape.png">
      </div><!--redTop-->

      <p class="text-center textoRodape">Automação Manutenção e Montagem de Equipamentos</br>Pensaremos no melhor para você</p>
      <a class="btn btn-block btn-social btn-facebook" id="socialf" href="https://www.facebook.com/exactcontrol" target="_blank">
        <span class="fab fa-facebook"></span> Siga-nos no Facebook
      </a>
      <a class="btn btn-block btn-social btn-linkedin" id="socialf" href="https://www.linkedin.com/company/exactcontrol-automation/" target="_blank">
        <span class="fab fa-linkedin"></span> Siga-nos no LinkedIn
      </a>
    </div><!--footer-->
  </div><!--section8-->

</body>
</html>