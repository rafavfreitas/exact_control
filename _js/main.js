$(document).ready(function(){
    if ( $(window).width() < 580) {
      $('#logo').hide();
      $('#unidade').hide();
      
    }
  });

/* /////////////////////
Menu de Navegação - Evento para rolar a página.
//////////////////////
*/

  $(document).ready(function(){
    // Add scrollspy to <body>
    //$('body').scrollspy({target: ".navbar", offset: 50});   

      // Add smooth scrolling on all links inside the navbar
    $("#myNavbar a, #footer a[href='#section0']").on('click', function(event) {
      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();
        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
        });
      }  // End if
    });
  });



/* /////////////////////
Seção 1 - Inicio
////////////////////// 
*/
//Evento para controlar primeiro Carrosel
$(document).ready( function() {
    $('#carouselInicio').carousel({
		interval:   5000
	});
	
	var clickEvent = false;
	$('#carouselInicio').on('click', '.nav a', function() {
			clickEvent = true;
			$('.primeiroCarousel li').removeClass('active');
			$(this).parent().addClass('active');		
	}).on('slid.bs.carousel', function(e) {
		if(!clickEvent) {
			var count = $('.primeiroCarousel').children().length -1;
			var current = $('.primeiroCarousel li.active');
			current.removeClass('active').next().addClass('active');
			var id = parseInt(current.data('slide-to'));
			if(count == id) {
				$('.primeiroCarousel li').first().addClass('active');	
			}
		}
		clickEvent = false;
	});
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

/* /////////////////////
Seção 3 - Produtos
////////////////////// 

stagePadding: 50,
    loop:true,
    margin:20,
    itens: 3,
    autoWidth:true,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }

*/

$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
    margin:10,
    loop:true,
    autoWidth:true,
    nav:true,
    items:4      
    
  });
});



/* /////////////////////
Seção 4 - Serviços
////////////////////// 
*/

  //Evento para o Collapse dos serviços

  $(document).ready(function(){
    $("#demo1").on("hide.bs.collapse", function(){
      $(".demo1").html('<span class="glyphicon glyphicon-plus"></span> Manutenção');
    });
  
    $("#demo1").on("show.bs.collapse", function(){
      $(".demo1").html('<span class="glyphicon glyphicon-minus"></span> Manutenção');
      $('#demo2').collapse('hide');
      $('#demo3').collapse('hide');
      $('#demo4').collapse('hide');
      $('#demo5').collapse('hide');
    });
  });

  
  $(document).ready(function(){
    $("#demo2").on("hide.bs.collapse", function(){
      $(".demo2").html('<span class="glyphicon glyphicon-plus"></span> Instalações');
    });
  
    $("#demo2").on("show.bs.collapse", function(){
      $(".demo2").html('<span class="glyphicon glyphicon-minus"></span> Instalações');
      $('#demo1').collapse('hide');
      $('#demo3').collapse('hide');
      $('#demo4').collapse('hide');
      $('#demo5').collapse('hide');
    });
  });

  $(document).ready(function(){
    $("#demo3").on("hide.bs.collapse", function(){
      $(".demo3").html('<span class="glyphicon glyphicon-plus"></span> Análises');
    });
  
    $("#demo3").on("show.bs.collapse", function(){
      $(".demo3").html('<span class="glyphicon glyphicon-minus"></span> Análises');
      $('#demo1').collapse('hide');
      $('#demo2').collapse('hide');
      $('#demo4').collapse('hide');
      $('#demo5').collapse('hide');
    });
  });

  $(document).ready(function(){
    $("#demo4").on("hide.bs.collapse", function(){
      $(".demo4").html('<span class="glyphicon glyphicon-plus"></span> Service EDC');
    });
  
    $("#demo4").on("show.bs.collapse", function(){
      $(".demo4").html('<span class="glyphicon glyphicon-minus"></span> Service EDC');
      $('#demo1').collapse('hide');
      $('#demo2').collapse('hide');
      $('#demo3').collapse('hide');
      $('#demo5').collapse('hide');
    });
  });

  $(document).ready(function(){
    $("#demo5").on("hide.bs.collapse", function(){
      $(".demo5").html('<span class="glyphicon glyphicon-plus"></span> Service Enerpac');
    });
  
    $("#demo5").on("show.bs.collapse", function(){
      $(".demo5").html('<span class="glyphicon glyphicon-minus"></span> Service Enerpac');
      $('#demo1').collapse('hide');
      $('#demo2').collapse('hide');
      $('#demo3').collapse('hide');
      $('#demo4').collapse('hide');
    });
  });



/* /////////////////////
Seção 5 - Fornecedores
////////////////////// 
*/


//Redimensionar Carousel - Caption
  $(document).ready(function() {
    $('.carousel .carousel-caption').css('zoom', $('.carousel').width()/1250);
  });

  $(window).resize(function() {
    $('.carousel .carousel-caption').css('zoom', $('.carousel').width()/1250);
  });



/* /////////////////////
Seção 6 - Localização
////////////////////// 
*/


$(document).ready(function(){
  $(".map-container").click(function() {
    $("#iframeMaps").addClass("clicked");
    $("#iframeMaps").removeClass("maps");
  });
});

$(document).ready(function(){
  $( ".map-container" ).mouseleave(function() {
    $("#iframeMaps").removeClass("clicked");
    $("#iframeMaps").addClass("maps");
  });
});

/* /////////////////////
Seção 7 - Contato
////////////////////// 
*/

$(document).ready(function(){
        $('#loadGif').hide();
        $('#sucessoEnvio').hide();
        $('#erroEnvio').hide();
        $('.imputForm').hide();
            $('#formularioContato').submit(function(){  //Ao submeter formulário
                $('#loadGif').show();
                $('#sucessoEnvio').hide();
                $('#erroEnvio').hide();
                var nome=$('#nome').val();
                var empresa=$('#empresa').val();
                var telefone=$('#telefone').val();
                var email=$('#email').val();
                var comentario=$('#comentario').val();
                $.ajax({            //Função AJAX
                    url:"email.php",                    //Arquivo php
                    type:"post",                            //Método de envio
                    data: "nome="+nome+"&empresa="+empresa+"&telefone="+telefone+"&email="+email+"&comentario="+comentario,   //Dados
                    success: function (result){             //Sucesso no AJAX
                        if(result==0){
                            $('#loadGif').hide();
                            $("#erroEnvio").html("<p class='text-center'>Erro de inserção na base de dados.</p>");
                            $('#erroEnvio').show();
                            $('#erroEnvio').addClass('animated shake');
                        }if(result==1){
                            $('#loadGif').hide();
                            $('#fieldsetForm').attr("disabled", "disabled");
                            $('.imputForm').show();
                            $(".form-group").addClass("has-success has-feedback");
                            //$(".imputForm").append("<span class='glyphicon glyphicon-ok form-control-feedback'></span>");
                            
                            $('#sucessoEnvio').show();
                            $('#sucessoEnvio').addClass('animated shake');                   
                        }if (result ==2){
                            $('#loadGif').hide();
                            $("#erroEnvio").html("<p class='text-center'>Ops, Um dos contatos não foi enviado.</p>");
                            $('#erroEnvio').show();
                            $('#erroEnvio').addClass('animated shake');
                        }if (result !=0 && result != 1 && result != 2){
                            $('#loadGif').hide();
                            $("#erroEnvio").html("<p class='text-center'>Ops, ocorreu um pequeno erro no envio do e-mail, mas não tem problema.</br>Você ainda pode ligar para nosso número: (81) 3471-6198.</p>");
                            $('#erroEnvio').show();
                            $('#erroEnvio').addClass('animated shake');
                        }  
                    }
                });
            return false;   //Evita que a página seja atualizada
            });
});

/*
*/

function formatar(mascara, documento){
  var i = documento.value.length;
  var saida = mascara.substring(0,1);
  var texto = mascara.substring(i)
  
  if (texto.substring(0,1) != saida){
            documento.value += texto.substring(0,1);
  }
  
}

  //Código para quando deslizar a página ele aparecer os elementos.
  $(window).scroll(function() {
    $(".esconderParaDeslizar").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });



//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//Exemplos
/*

//Evento para redimensionar Carousel para Mobile(Ainda não testeado)
  if ($(window).width() < 960) {
    $('#myCarousel .hide-on-mobile').removeClass('carousel-caption').addClass('hide');
  }


$(document).ready(function(){
  $("#section5").css("height", $(window).height());
});


.toggleClass()

$('.map-container')
  .click(function(){
      $(this).find('iframe').addClass('clicked')})
  .mouseleave(function(){
      $(this).find('iframe').removeClass('clicked')});


/* Play e Pause Video
  $(document).ready(function(){
    $('.playVideo').click(function(){
      $('.playVideo').hide();
      $("#video1")[0].play();
    });
});

  $(document).ready(function(){
    $('.playVideo2').click(function(){
      $('.playVideo2').hide();
      $("#video2")[0].play();
    });
});

$(document).ready(function(){
    $('#painel video').click(function(){
      var vid = document.getElementById("video1"); 
      vid.pause();
      $('.playVideo').show();
    });
});

function pauseVideo(id){
  id.pause();
  if (id.id == "video1") {
    $('.playVideo').show();
  }if (id.id == "video2") {
    $('.playVideo2').show();
  }  
}

*/